import bodyParser from 'body-parser';
import express from 'express';
import {connect} from 'mongoose';
import { tasksRoutes } from './router/tasks';
import { listsRoutes } from './router/lists';
import { usersRoutes } from './router/users';


async function run(): Promise<void> {
    // Connect to MongoDB
    const app=express();

    app.use(function (req, res, next) {
        // Website you wish to allow to connect
      
        res.setHeader("Access-Control-Allow-Origin", "*");
        // Request methods you wish to allow
        res.setHeader(
          "Access-Control-Allow-Methods",
          "GET, POST, OPTIONS, PUT, PATCH, DELETE"
        );
      
        // Request headers you wish to allow
        res.setHeader("Access-Control-Allow-Headers", "*");
    
        next();
      });
     
      app.use(bodyParser.urlencoded({ extended: true }));
      app.use(bodyParser.json())

      
      app.use('/tasks',tasksRoutes);
      app.use('/lists',listsRoutes);
      app.use('/users',usersRoutes);
    
    try{
        await connect('mongodb://localhost:27017/scrumtask', {useNewUrlParser: true, useUnifiedTopology: true});
            console.log('Connection success');
            app.listen(5000);
            console.log("server running on port http://localhost:5000");
            app.get('/', function(req, res) {
                res.send('<h1>Server running with MongoDB</h1><br><i>To test, send post request with postman  to http://localhost:3000/ page.</i><br><br>@Rihani' );
              });

    }catch(err){
        console.log('Connection Fail',err);
    }
    

    
    
}
module.exports=run();