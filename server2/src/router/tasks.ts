import express from 'express';
import "reflect-metadata";
import Task from '../entity/Task';
import auth from '../middleware/auth';

const router = express.Router();


router.get('/', async (req,res)=>{
    const tasks =await Task.find()
    res.json(tasks);
     
});

router.post('/',auth, async (req, res)=>{
    
    if(!req.body.title)
    res.status(500).json("no title found");
    else
    {           
         //@ts-ignore
        req.body.createdBy = req.userId
        
            let task = new Task(req.body);
            await task.save();
            const tasks= await Task.findById(task._id).populate({ path: 'createdBy', select: 'name email' }).populate({ path: 'assignTo', select: 'name' });
            res.json(tasks);
        
    }
    
});


router.get('/:id',async (req,res)=>{
    const id=req.params.id
    const tasks = await Task.findOne({_id: id});
    if(tasks)
       res.json(tasks);
        
    else
        res.json("i dont have this task id");
    
});

router.delete('/:id',auth,async (req,res)=>{
       // @ts-ignore
    let userRole = req.userRole
    const id =req.params.id
    const tasks =await Task.findOne({_id: id});
    if(userRole==="Admin"){
        await Task.deleteOne(tasks);
            if(tasks)
            res.json(tasks);
                
            else
            res.json("i dont have this list id");
    }else{
        res.json("Sorry you don't have access to delete this task")
    }
    
    
});

router.put('/:id',async (req,res)=>{
    if(!req.body.title && !req.body.idList)
    res.json("no title found");
    else
    {
    const id =req.params.id
    const{title,idList}=req.body
    const resp = await Task.updateOne({
        _id:id,
    },
    {
        $set:{
            title:title,
            idList:idList
        }
    })
    const tasks= await Task.findById(id).populate({ path: 'createdBy', select: 'name email' }).populate({ path: 'assignTo', select: 'name' });
    if(tasks)
       res.json(tasks);
        
    else
        res.json("i dont have this task id");
    }

});
export {
    router as tasksRoutes
}