import express from 'express';
import "reflect-metadata";
import User from '../entity/User';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import Task from '../entity/Task';
import { login, register } from '../control/user.control';

const router = express.Router();

router.get('/',async (req,res)=>{
    const users = await User.find({});
          res.json(users);
     
  });

router.post('/login',login);


router.post('/register', register);
  

  router.get('/:id',async (req,res)=>{
    const id=req.params.id
    const users = await User.findOne({_id: id});
    if(users)
       res.json(users);
        
    else
        res.json("i dont have this user id");
    
});

router.delete('/:id',async (req,res)=>{
    const id =req.params.id
    const users =await User.findOne({_id: id});
    await Task.deleteMany({createdBy:id});
    await User.deleteOne({_id: id});
    if(users)
       res.json(users);
        
    else
        res.json("i dont have this user id");
    
});
router.put('/:id',async (req,res)=>{
    const id =req.params.id
    const{name,email}=req.body
    const resp = await User.updateOne({
        _id:id,
    },
    {
        $set:{
            name:name,
            email:email
        }
    })
    if(resp)
       res.json(resp);
        
    else
        res.json("i dont have this user id");

});
router.put('/setrole/:id',async (req,res)=>{
    const id =req.params.id
    
    const{role}=req.body
    if(!req.body.role)
    res.status(500).json("no role found");
    else
    {
        const resp = await User.updateOne({
            _id:id,
        },
        {
            $set:{
                role:role
            }
        })
        if(resp){
            res.json(resp);
        }else{
            res.send("i dont have this user id");
        }
            
        

}});

  export {
    router as usersRoutes
}