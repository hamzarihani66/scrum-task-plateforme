import express from 'express';
import "reflect-metadata";
import Task from '../entity/Task';
import List from '../entity/List';
import auth from '../middleware/auth';

const router = express.Router();

router.get('/', async (req,res)=>{
    const lists =await List.find();
    res.json(lists);
     
});
router.post('/',auth, async (req, res)=>{
    // @ts-ignore
    let userRole = req.userRole
    if(userRole==="Admin"){
        if(!req.body.name)
        res.json("no name found");
        else
        {
            let list = new List(req.body);
            await list.save();
            res.json(list);
        }
    }else{
        res.json("Sorry you don't have access to add list")
    }
    
});
router.get('/:id',async (req,res)=>{
    const id=req.params.id
    const lists = await List.findOne({_id: id});
    if(!lists)
        res.json("i dont have this list id")
    else
        res.json(lists);
    
});


router.delete('/:id',auth,async (req,res)=>{
     // @ts-ignore
    let userRole = req.userRole
    const id =req.params.id
    if(userRole==="Admin"){
        await Task.deleteMany({idList:id});
        await List.deleteOne({_id:id});
    
        res.send("i dont have this list id")
    }else{
        res.json("Sorry you don't have access to delete this list")
    }
    
});
// access to tasks in list
router.get('/:id/tasks',async (req,res)=>{
    
    const id =req.params.id;
    const list =await List.findOne({_id: id});
    const tasks = await Task.find({idList:id}).limit(3).populate({ path: 'createdBy', select: 'name email' }).populate({ path: 'assignTo', select: 'name' }).populate({ path: 'idList', select: 'name' });
    if(list)
        res.json(tasks.sort(function (a:any, b:any) {
            if (a.dueDate < b.dueDate) {
              return -1;
            } else {
              return 1;
            };
           }));
    else
        res.send("i dont have task in this list id");
    
});


export {
    router as listsRoutes
}