import jwt from 'jsonwebtoken';

const auth = async (req:any,res:any,next:any)=>{
    try{

        const token= req.headers.authorization.split(" ")[1];
        let decodedData : any ;
        if(token){
            decodedData = jwt.verify(token,'test');
            req.userRole = decodedData?.role;
            req.userId = decodedData?.id;
        }else{
            decodedData =jwt.decode(token);
            req.userRole = decodedData?.role;
        }
        next();
    }catch(error){
        console.log(error);
    }
}
export default auth;