import express from 'express';
import User from '../entity/User';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

export const login = async (req:any,res:any)=>{
    const {email, password}=req.body;
    try{
            const existeUser = await User.findOne({email});
            
            if(!existeUser){
                 return res.json({success:false,message:"user doesn't existe"});
            }
           const isPasswordCorrect =await bcrypt.compare(password,existeUser.password);
            if(!isPasswordCorrect){
                return res.json({success:false,message:"invalide password"});
            }
            const token=jwt.sign({email:existeUser.email, id :existeUser._id, role:existeUser.role},'test');
            let user = existeUser.toObject()
            delete user.password
            res.status(200).json({success:true,user, token});

    }catch(error){
        res.status(500).json({message:'error'});
    }   
}

export const register = async (req:any,res:any)=>{
    const {email, password, name,role}=req.body;
    try{
            const existeUser = await User.findOne({email});
            
            if(existeUser){
                 return res.json({success:false,message:"user already existe"});

            }
            const hashedPassword =await bcrypt.hash(password,12);
            const result= await User.create({email,password: hashedPassword,name,role});
            res.json({success:true,result});

        

    }catch(error){
        res.status(500).json({message:'error'});
    }
    

    
}