import {Schema, model} from 'mongoose';


const TaskSchema = new Schema({
   
    title:{
        type:String,
        default:'No Title',
        required:true
    },
     idList:{
         type:Schema.Types.ObjectId,
         ref:"List",
         required:true
         
     },
     dateCreation:{
        type:Date,
        default: Date.now
    },
    dueDate:{
        type:Date,
        default: Date.now,
        required:true
    },
    createdBy:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required:true
        
    },
    assignTo:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required:true
        
    }
})
export default model('Task',TaskSchema);
