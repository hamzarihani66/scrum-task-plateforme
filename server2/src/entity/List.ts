import {Schema,model} from 'mongoose';
import TaskSchema from "../entity/Task"

export const ListSchema = new Schema({
   
    name:{  
        type:String,
        default:'No name',
        required:true
    }
})
export default model('List',ListSchema)
