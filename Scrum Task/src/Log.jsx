import React from "react";
import "./Log.css";
import { Login, Register } from "./components/login/index";

class Log extends React.Component {
  constructor(props) {
    super(props); 
    this.props = props
    this.state = {
      isLogginActive: true
    };
  }

  componentDidMount() {
    //Add .right by default
    this.rightSide.classList.add("right");
  }

  changeState() {
    const { isLogginActive } = this.state;

    if (isLogginActive) {
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    } else {
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState(prevState => ({ isLogginActive: !prevState.isLogginActive }));
  }

  render() {
    const { isLogginActive } = this.state;
    const current = isLogginActive ? "Register" : "Login";
    const currentActive = isLogginActive ? "login" : "register";
    return (
      <div className="Appp">
        <div className="loginn">
          <div className="containerr" ref={ref => (this.container = ref)}>
            {isLogginActive && (
              <Login containerRef={ref => (this.current = ref)} props={this.props} />
            )}
            {!isLogginActive && (
              <Register containerRef={ref => (this.current = ref)} props={this.props}/>
            )}
          </div>
          <RightSide
            current={current}
            currentActive={currentActive}
            containerRef={ref => (this.rightSide = ref)}
            onClick={this.changeState.bind(this)}
          />
        </div>
      </div>
    );
  }
}

const RightSide = props => {
  return (
    <div
      className="right-sidee"
      ref={props.containerRef}
      onClick={props.onClick}
    >
      <div className="inner-containerr">
        <div className="textt">{props.current}</div>
      </div>
    </div>
  );
};

export default Log;
