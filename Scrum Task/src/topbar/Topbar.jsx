import React from "react";
import "./topbar.css";
// import pdf from './pdf.jpg';
import { Link } from "react-router-dom";
import Sidebar from '../sidebar/Sidebar';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';
import { NotificationsNone} from "@material-ui/icons";
import Avatar from '../Avatar/Avatar';
import { useState ,useEffect} from "react";
import decode from 'jwt-decode';

export default function Topbar() {

  const [user,setUser]=useState(JSON.parse(localStorage.getItem('profile')));

  
  useEffect(()=>{
    const token =user?.token;
    setUser(JSON.parse(localStorage.getItem('profile')));
    if (token) {
      const decodedToken = decode(token);

      if (decodedToken.exp * 1000 < new Date().getTime());
    }
    setUser(JSON.parse(localStorage.getItem('profile')))
    
  },[user?.token]);


  return (
    <div className="topbar">
      <div className="topbarWrapper">
      <div className="topLeft"><Sidebar/></div>
        <div className="topLeft">
        
            <Link to="/" className="link">
            <span className="logo">Scrum Task</span>
            </Link>

         
        </div>
        
        <div className="topRight">
          {user ? (
              <div className="topRight">
                  <div className="topbarIconContainer">
                  <NotificationsNone />
                  <span className="topIconBadge">5</span>
                  </div>
                  <div className="topbarIconContainer">
                  <ChatOutlinedIcon />
                  <span className="topIconBadge">1</span>
                  </div>
                
                {/* <img src={pdf} alt="" className="topAvatar" /> */}
                  
            <Avatar/>
              </div>
          ):(  <Link to="/login" className="btn btn-primary link" role="button" aria-pressed="true">Login</Link>
          )}
          
          
        </div>
      </div>
    </div>
  );
}
