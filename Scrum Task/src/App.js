import React ,{Suspense} from 'react';
import { BrowserRouter,Route, Switch } from 'react-router-dom';
import './App.css';
// import Home from './components/Home/Home'
// import Log from './Log'
// import Board from './components/Board/Board'
// import Contact from './components/ContactUs/Contact';
// import List from './components/Board/List';
// import Users from './components/users/users';
// import Profil from './components/profil/profil';
const Home = React.lazy(() => import('./components/Home/Home'));
const Log =  React.lazy(() => import('./Log'));
const Board =  React.lazy(() => import('./components/Board/Board'));
const Contact =  React.lazy(() => import('./components/ContactUs/Contact'));
const Users =  React.lazy(() => import('./components/users/users'));
const Profil =  React.lazy(() => import('./components/profil/profil'));



function App() {

  return (
    <BrowserRouter>
    
    <Suspense fallback={<div className="d-flex justify-content-center cen">
                          <div className="spinner-grow text-primary dot" role="status">
                            <span className="sr-only">Loading...</span>
                          </div>
                        </div>
}>
    <div className="container">
        
        <Switch>
      <Route exact path="/Home" component={Home}/>
      <Route exact path="/" component={Home}/>
      <Route exact path="/login" component={Log}/>
      <Route exact path="/Board" component={Board}/>
      <Route exact path="/ContactUs" component={Contact}/>
      <Route exact path="/users" component={Users}/>
      <Route exact path="/myprofil" component={Profil}/>
      <Route exact path="*" component={Home}/>


       </Switch>
      </div>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
