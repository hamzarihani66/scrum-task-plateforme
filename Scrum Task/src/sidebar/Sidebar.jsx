import "./sidebar.css";
// import ContactsOutlinedIcon from '@material-ui/icons/ContactsOutlined';
import { Link } from "react-router-dom";
import { useState } from "react";
import * as AiIcons from 'react-icons/ai';
import { IconContext } from 'react-icons';

export default function Sidebar() {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  
  return ( 
  <IconContext.Provider value={{ color: '#fff' }}>
  <div className='navbar'>
    <Link to='#' className='menu-bars'>
      <i className="fas fa-bars" onClick={showSidebar} ></i>
    </Link>
  </div>
  <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
    <ul className='nav-menu-items' onClick={showSidebar}>
      <li className='navbar-toggle'>
        <Link to='#' className='menu-bars'>
          <AiIcons.AiOutlineClose />
        </Link>
      </li>
      <div className="sidebar">
      <div className="sidebarWrapper">
        <div className="sidebarMenu">
          <h3 className="sidebarTitle">Dashboard</h3>
          <ul className="sidebarList">
            <Link to="/Home" className="linkk">
            <li className="sidebarListItem active">
              <i class="fas fa-home sidebarIcon">&nbsp;</i>
              Home
            </li>
            </Link>
            <Link to="/Board" className="linkk">
            <li className="sidebarListItem">
              <i class="fas fa-tachometer-alt sidebarIcon">&nbsp;</i>
               Board
            </li>
            </Link>
            <Link className="linkk">
            <li className="sidebarListItem">
            <i className="fas fa-inbox sidebarIcon">&nbsp;</i>
              Message Box
            </li>
            </Link>
            {/* <Link to="/ContactUs" className="linkk">
            <li className="sidebarListItem">
              <ContactsOutlinedIcon className="sidebarIcon" />
              Contact us
            </li>
            </Link> */}
            
          </ul>
        </div>
        <div className="sidebarMenu">
          <h3 className="sidebarTitle">Quick Menu</h3>
          <ul className="sidebarList">
            <Link to="/users" className="linkk">
              <li className="sidebarListItem">
                <i className="fas fa-user sidebarIcon">&nbsp;</i>
                Users
              </li>
            </Link>
            <Link to="/About" className="linkk">
            <li className="sidebarListItem">
              <i className="fas fa-question-circle sidebarIcon">&nbsp;</i>
              About us
            </li>
            </Link>
            
            
          </ul>
        </div>
        
        
      </div>
    </div>
    </ul>
  </nav>
</IconContext.Provider>

);
}
