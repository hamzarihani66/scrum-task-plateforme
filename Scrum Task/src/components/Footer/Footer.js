import React from 'react';
import { Link } from 'react-router-dom';
import './Footer.css';
function Footer () {
   
        return ( 
            <footer>
                <div class="footer-content">
                    <h3>Scrum Tasks </h3>
                    <p>Winshot helps consumer brands and retailers empower store performance to increase conversion</p>
                    <ul class="socials">
                        <li><a href="https://www.facebook.com/winshotapp"><i class="fab fa-facebook-square"></i></a></li>
                        <li><Link to="#"><i class="fab fa-twitter"></i></Link></li>
                        <li><Link to="#"><i class="fab fa-youtube"></i></Link></li>
                        <li><a href="https://www.linkedin.com/company/winshot/?originalSubdomain=tn"><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="footer-bottom">
                    <p>copyright &copy; 2021 Scrum Tasks. designed by <span>H.Rihani</span></p>
                </div>
        </footer>
         );
    
}
 
export default Footer;