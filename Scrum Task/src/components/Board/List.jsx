import React,{Component} from 'react';
import './List.css';
import Task from './Task';
import AddModal from '../forms/addTask';
import  axios from 'axios';

class List extends Component {
    constructor(props){
        super(props);
        this.state = {
            name:'',
            tasks:[],
            loadingTasks:true
        }
        this.getTasks=this.getTasks.bind(this);
        this.deleteTask=this.deleteTask.bind(this);
    }   
    componentDidMount = ()=>{
        this.getTasks();
        

      }  
      
      
 
    getTasks = () => {
        
        axios.get(`/lists/${this.props.list._id}/tasks`)
        .then((r)=> {
            this.setState({
                tasks: r.data
            })
        }).then(()=>{
          this.setState({
            loadingTasks:false
        })
      })
        .catch((e)=>{
            this.setState({
              loadingTasks:false,
              err2: e
            })
        })
      }
      

      deleteTask = (idList,_id) => {
        axios.delete('/tasks/'+_id,{headers:{
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
        }})
        .then((response)=>{
          if(JSON.parse(localStorage.getItem('profile')).user.role!=="Admin"){
            alert("Sorry you don't have access to delete this task")
          }
            
        })
        .then(()=>{
          this.getTasks()
        })
        .catch(function (error) {
          console.log(error);
        });
      
      }

      addNewTask = (newTask) => {
        
      this.setState({tasks:[...this.state.tasks,...[newTask]]})

      }
      
      componentWillReceiveProps(){
        this.getTasks()
      }
    
    render() {
        
         
        return (
            <div className="container-fluid">
                <div className="row ">
                    <div className="col">
                        <div>
                        <table className="table2 .mcell">
                            <tr>
                            <td className="col col-6">
                            <i className="fas fa-clipboard-list">&nbsp;{this.props.list.name}</i>
                            
                            
                            </td>
                            <td className="col col-6 le">
                                <AddModal
                                addTask={this.props.addTask}
                                idList={this.props.list._id}
                                addNewTask={this.addNewTask}
                                history={this.props.history}
                                
                                />
                            </td>
                            <td className="col col-2 le">
                                <button type="button" onClick={()=>{if(window.confirm('Are you sure to delete this LIST ?')){this.props.deleteList(this.props.list._id)}}} className="btn btn-danger btn-sm "><i className="fa fa-trash" ></i></button>
                            </td>
                        </tr>
                        </table>
                        </div>
                        <br/>
                        {this.state.tasks.map(task=>(
                                <Task 
                                key={task._id}
                                task={task}
                                idList={this.props.list._id}
                                addTask={this.props.addTask}
                                deleteTask={this.deleteTask.bind(this)}
                                updateATask={this.props.updateATask}
                                />
                            ))}
                            
                        </div>
                
                </div>
                
            </div>
         );
    }
}
 
export default List;