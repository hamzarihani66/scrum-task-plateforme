import React from 'react';
import './Board.css';
import List from './List';
import axios from 'axios';
import Topbar from '../../topbar/Topbar';
import SimpleReactValidator from 'simple-react-validator';

class Board extends React.Component{ 
  constructor(props){
    super(props);
    this.validator = new SimpleReactValidator();
    this.state={
      lists:[],
      loadingLists:true,
      newList:""
  }
 
  
    this.updateInput=this.updateInput.bind(this);
    this.getLists=this.getLists.bind(this);
    this.handleaddlist=this.handleaddlist.bind(this);
    this.deleteList=this.deleteList.bind(this);

  }  
  

  
      updateInput(value){
        if(value){
          this.setState({
            newList:value
          });
        }else{
          this.setState({
            newList:""
          });
        }
        }
  
      //backend function

    
    handleaddlist = (e) =>{
      e.preventDefault();
      if(this.validator.allValid()){
        axios.post('/lists', {
        name:this.state.newList,
      },{headers:{
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
      }}).then((response)=> {
        if(JSON.parse(localStorage.getItem('profile')).user.role==="Admin"){
          const newList = this.state.lists
          this.setState({newList : '', lists:[...newList,...[response.data]]})
          
        }else{
          alert("Sorry you don't have access to add this list")}
      })
      .catch((error)=> {
        console.log(error);
      });
    }else{
      this.validator.showMessages();
      this.forceUpdate();
    }}

    componentDidMount = ()=>{
      this.getLists();
      const profil=localStorage.getItem('profile');
    if(profil===null){
      this.props.history.push('/login')
    }
      
    }

    getLists = () => {
      axios.get('/lists')
      .then((r)=> {
          this.setState({
              lists: r.data
          })
      }).then(()=>{
        this.setState({
          loadingLists:false
      })
    })
      .catch((e)=>{
          this.setState({
            loadingLists:false,
            err2: e
          })
      })
    }
    componentWillReceiveProps(){
            this.getLists()
          }

   updateATask = (taskUpdate) => {
      this.getLists()
      this.forceUpdate()
      }
      

    deleteList = (id) => {
      axios.delete('/lists/'+id,{headers:{
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
      }})
      .then((response) =>{
        if(JSON.parse(localStorage.getItem('profile')).user.role!=="Admin"){
          alert("Sorry you don't have access to delete this list")
        }
          
      })
      .then(()=>{
        this.getLists();
      })
      .catch(function (error) {
        console.log(error);
      });
    
    }
    
    
       render(){
        
        return ( 
              <div className="cover"><Topbar/>
            <div className="App">
              <form>
                      <div className="col">
                              <input type="text"
                              id="resetInput"
                              name="name" 
                              value={this.state.newList} 
                              onChange={e=> this.updateInput(e.target.value)}
                              className="input-list" 
                              placeholder="Name of list"
                              />
                              
                              &nbsp;

                          <button type="submit" onClick={(e) => this.handleaddlist(e)} className="btn btn-success addB" >ADD LIST</button>
                          {this.validator.message('name list',this.state.newList,'required|min:3|max:20',{ className: 'text-danger' })}
                          </div></form>
                      

                    <div className="row">
                        {this.state.lists.map(list => (
                          <div className="col">
                            <div className="col-sm mcell mcolor2">
                            <div className="mcell-title story">
                            <List 
                            key={list._id}
                            list={list}
                            deleteList={this.deleteList.bind(this)}
                            updateATask={this.updateATask.bind(this)}
                            history={this.props.history}
                            
                            />
                            </div></div>
                          </div>
                        ))}
              
            </div>
            </div>
            </div>
         );
}
}
export default Board;