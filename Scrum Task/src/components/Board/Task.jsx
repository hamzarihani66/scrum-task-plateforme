import './Task.css';
// import pdp from './img/pdp.jpg'
import React, { Component } from 'react';
import moment from 'moment';
import $ from 'jquery';
import 'jquery-ui-dist/jquery-ui';
import ModalExample from '../forms/modal';


class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentWillReceiveProps(){
  
    $(".mcell-task").draggable({
      appendTo: "body",
      cursor: "move",
      helper: 'clone',
      revert: "invalid"
  });
  
  $(".mcell").droppable({
      tolerance: "intersect",
      accept: ".mcell-task",
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      drop: function(event, ui) {        
          $(this).append($(ui.draggable));
          console.log($(this).find("li").attr('_id'))
      }
  });
  }

  
  render() { 
    return ( <div>
      <li  className="mcell-task" >
          <span className="task-name">
            <span>{this.props.task.title}</span>
            <i id="delete" className="fas fa-times px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-200 text-black-800" onClick={() => {if(window.confirm('Are you sure to delete this task ?')){this.props.deleteTask(this.props.idList,this.props.task._id)};}}></i>
          </span>
          <br/>
          <div className="task-due">
          <span className="task-due"><i className="fas fa-clock">&nbsp;{moment(this.props.task.dueDate).format("DD.MM.YYYY")}</i></span>
          <div><br/>
              <span className="task-contributors"><table className="row"><tr>
              <th><i className="fas fa-user">By:{this.props.task.createdBy.name}</i></th>&nbsp;<th><i className="fas fa-user">AssignTo:{this.props.task.assignTo.name}</i></th>
              </tr></table>
              </span>
          </div>
        </div>
        <div className="button-edit">
          <ModalExample
          propContent={this.props.task}
          idList={this.props.idList}
          updateATask={this.props.updateATask}
          />
        </div>
        </li>
    </div> );
  }
}
 
export default Task;