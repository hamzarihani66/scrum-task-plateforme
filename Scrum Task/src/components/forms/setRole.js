import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Input,FormGroup,Label } from 'reactstrap';
import SimpleReactValidator from 'simple-react-validator';
import axios from 'axios';
class SetRole extends React.Component {
  constructor(props) {
    super(props);
    this.validator=new SimpleReactValidator();
    this.state = {
      modal: false,
      role:this.props.role,
      id:this.props.id,
      user:[]
    };

    this.toggle = this.toggle.bind(this);
  }
  handleInput(e) { 
    this.setState({
     [e.target.name]: e.target.value
    })
}
componentDidMount(){
  this.getUsers();
}

getUsers(){
  axios.get(`/users`)
  .then((r)=> {
      this.setState({
          user: r.data
      })
  })
  .then((r)=>{
    
  })
  .catch((e)=>{
      this.setState({
          err: e
      })
  })
}

handleClick = id => {
  if(this.validator.allValid()){
  axios.put(`/users/setrole/${id}`, {
    role:this.state.role,
    
  },{headers:{
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
  }})
  .then((response)=> {
      this.getUsers();
      this.toggle();
 
  })
  .catch((error)=> {
    console.log(error);
  });
}else{
  this.validator.showMessages();
  this.forceUpdate();
}
  
}
  toggle() {
    if(JSON.parse(localStorage.getItem('profile')).user.role!=="Admin"){
      alert("You don't have access to give role")
    }else{
      this.getUsers()
    this.setState({
      modal: !this.state.modal
    });
    }
    
  }

  render() {
    const {role,id} = this.state;
    
    
    return (
      <div>
        <button className="btn px-2 inline-flex  leading-5 font-semibold rounded-full bg-blue-200 text-blue-800 " onClick={this.toggle}><i className="fas fa-medal"/></button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader className="btn btn-primary" >
            Set Role
          </ModalHeader>
          <ModalBody>
          <FormGroup>
            <Label for="role">Role</Label>
            <Input type="select" name="role" id="role" value={role} onChange={this.handleInput.bind(this)}>
                <option value="Team Developer">Team Developer</option>
                <option value="Admin">Admin</option>
                
            </Input>
            {this.validator.message('role', this.state.role, 'required',{ className: 'text-danger' })}
          </FormGroup>
        </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={()=>this.handleClick(id)}>Update</Button>
            <Button color="secondary" onClick={this.toggle}>Close</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default SetRole;