import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Input,FormGroup,Label } from 'reactstrap';
import moment from 'moment';
import SimpleReactValidator from 'simple-react-validator';
import axios from 'axios';
class ModalExample extends React.Component {
  constructor(props) {
    super(props);
    this.validator=new SimpleReactValidator();
    this.state = {
      modal: false,
      title:this.props.propContent.title,
      idList:this.props.idList,
      lists:[],
      
    };

    this.toggle = this.toggle.bind(this);
  }
  handleInput(e) { 
    this.setState({
     [e.target.name]: e.target.value
    })
}
componentDidMount(){
  this.getLists();
}
getLists(){
  axios.get('/lists')
  .then((r)=> {
      this.setState({
          lists: r.data,
          err:''
      })
      
  })
  .then((r)=>{
    
  })
  .catch((e)=>{
      this.setState({
          err: e
      })
  })
}

handleClick = id => {
  if(this.validator.allValid()){
  axios.put(`/tasks/${id}`, {
    title:this.state.title,
    idList:this.state.idList,
  })
  .then((response)=> {
      this.toggle();
      this.props.updateATask(response.data)

  
  })
  .catch((error)=> {
    console.log(error);
  });
}else{
  this.validator.showMessages();
  this.forceUpdate();
}
  
}
  toggle() {
    this.getLists();
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
      const {title,lists} = this.state;
      const {propContent} = this.props;

    let listContent;
    if(!lists)
      listContent = <option value="">Loading...</option>
    else{
      listContent = lists.map((list,index)=>(
              <option key={index} value={list._id}>{list.name}</option>
      ))
    }
    
    
    return (
      <div>
        <button className="btn px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-blue-200 text-blue-800 " onClick={this.toggle}><i className="fas fa-edit"/></button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader className="btn btn-primary" >
            Update Task 
          </ModalHeader>
          <ModalBody>
          <FormGroup>
          <Label for="title">Task Title:</Label>
          <Input type="text" id="taskTitle" name="title" value={title} onChange={this.handleInput.bind(this)}/>
          {this.validator.message('title', this.state.title, 'required|min:3|max:40',{ className: 'text-danger' })}
        </FormGroup>
        <FormGroup>
            <Label for="idList">Status</Label>
            <Input type="select" name="idList" id="idList" onChange={this.handleInput.bind(this)}>
                <option value="">{propContent.idList.name}</option>
                {listContent}
            </Input>
            {this.validator.message('Status', this.state.idList, 'required',{ className: 'text-danger' })}
          </FormGroup>
          <hr/>
        <Label for="status">Info Task:</Label><br/>
            
              <i className="fas fa-calendar-alt"></i> Created Date: {moment(propContent.date).format("DD.MM.YYYY")}<br/>
              <i className="fas fa-clock"></i> Due Date: {moment(propContent.dueDate).format("DD.MM.YYYY")}<br/>
              <i className="fas fa-user"></i> Created by: {propContent.createdBy.name}<br/>
              <i className="fas fa-user"></i> Assign To: {propContent.assignTo.name}

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={()=>this.handleClick(propContent._id)}>Update</Button>
            <Button color="secondary" onClick={this.toggle}>Close</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default ModalExample;