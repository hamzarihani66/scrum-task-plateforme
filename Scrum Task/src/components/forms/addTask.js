import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Input,FormGroup,Label } from 'reactstrap';
import moment from 'moment';
import axios from 'axios';
import SimpleReactValidator from 'simple-react-validator';


class AddModal extends React.Component {
  constructor(props) {
    super(props);
    this.validator=new SimpleReactValidator();
    this.state = {
      modal: false,
      title:'',
      createdBy:'',
      dueDate:'',
      idList:'',
      loading:false,
      assignTo:'',
      users:[]
     
    };

    this.toggle = this.toggle.bind(this);

  }
  
  componentDidMount(){
    moment.locale('tn');
    const profil=localStorage.getItem('profile');
    if(profil===null){
      this.props.history.push('/login')
    }
  }
  
  handleInput(e) {
     this.setState({
      [e.target.name]: e.target.value
     })
    //  console.log(JSON.parse(localStorage.getItem('profile')).user._id);
    }

getUsers(){
  axios.get('/users')
  .then((r)=> {
      this.setState({
          users: r.data,
          err:''
      })
  })
  .then((r)=>{
    
  })
  .catch((e)=>{
      this.setState({
          err: e
      })
  })
}
  handleClick = event => {
    if (this.validator.allValid()) {
    axios.post('/tasks', {
      title:this.state.title,
      dueDate:this.state.dueDate,
      createdBy:this.state.createdBy,
      idList:this.props.idList,
      assignTo:this.state.assignTo
      
    },{headers:{
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
    }})
    .then((response)=> {
      
        this.toggle();
        this.setState({
          title:null,
          createdBy:null,
          dueDate:null,
          assignTo:null,
          loading:false
        })
        this.props.addNewTask(response.data)
        
     if(JSON.parse(localStorage.getItem('profile')).user.role!=="Admin"){
       alert("Sorry you don't have access to add new task")
     }
    })
    .catch((error)=> {
      console.log(error);
    });
  }else{
    this.validator.showMessages();
    this.forceUpdate();
  }
    
  }
  toggle() {
    if(JSON.parse(localStorage.getItem('profile')).user.role!=="Admin"){
      alert("You don't have access to add Task")
    }else{
    this.getUsers();
    this.setState({
      modal: !this.state.modal
    });}
  }
  

  render() {
    const {users} = this.state;
    let userContent;
    if(!users)
      userContent = <option value="">Loading...</option>
    else{
      userContent = users.map((user,index)=>(
              <option key={index} value={user._id}>{user.name}</option>
      ))
    }

    return (
      <div>
        <button className="btn btn-success btn-sm" ><i className="fas fa-plus-circle" onClick={this.toggle}></i></button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader className="btn btn-success" >
            Create a New Task 
          </ModalHeader>
          <ModalBody>
          <FormGroup>
          <Label for="title">Task Title(*):</Label>
          <Input type="text" name="title" id="taskTitle" onChange={this.handleInput.bind(this)}/></FormGroup>
          {this.validator.message('title', this.state.title, 'required|min:3|max:40',{ className: 'text-danger' })}
          
        <FormGroup>
            <Label for="assignTo">Assign to:</Label>
            <Input type="select" name="assignTo" id="assignTo" onChange={this.handleInput.bind(this)}>
                <option value="">Choose:</option>
                {userContent}
            </Input>
            {this.validator.message('assignTo', this.state.assignTo, 'required',{ className: 'text-danger' })}
          </FormGroup>
        
              <hr/>
              <i className="fas fa-calendar-alt"></i> Created Date: {moment().format('DD-MM-YYYY, h:mm:ss')} <br/>
              <i className="fas fa-clock"></i> Due Date: <input name="dueDate" id="dueDate" type="date" min={moment().format('YYYY-MM-DD')} controls={['date']}  onChange={this.handleInput.bind(this)}/>
              {this.validator.message('date', this.state.dueDate, 'required',{ className: 'text-danger' })}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.handleClick.bind(this)}><i className="fas fa-plus-circle"></i> Add</Button>
            <Button color="secondary" onClick={this.toggle}><i className="fas fa-times-circle"></i> Close</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default AddModal;