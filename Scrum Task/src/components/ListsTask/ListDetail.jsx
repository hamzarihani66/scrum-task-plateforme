import React from 'react';
// import Task from '../Board/Task';
import axios from 'axios';
import Topbar from '../../topbar/Topbar';
import SimpleReactValidator from 'simple-react-validator';

class ListDetail extends React.Component{ 
  constructor(props){
    super(props);
    this.validator = new SimpleReactValidator();
    this.state = {
        list:[{
            name:"",
            tasks:[]
    }]
        
    }
 
  
    this.getLists=this.getLists.bind(this);
    this.getTasks=this.getTasks.bind(this);


  }  
  


    

    componentDidMount = ()=>{
      this.getLists();
      const profil=localStorage.getItem('profile');
    if(profil===null){
      this.props.history.push('/login')
    }
      
    }

    getLists = (id) => {
      axios.get(`/lists/${id}`)
      .then((r)=> {
          this.setState({
              list: r.data
          })
      }).then(()=>{
        this.setState({
          loadingLists:false
      })
    })
      .catch((e)=>{
          this.setState({
            loadingLists:false,
            err2: e
          })
      })
    }
    

          getTasks = (idList) => {
        
            axios.get(`/lists/${idList}/tasks`)
            .then((r)=> {
                this.setState({
                    tasks: r.data
                })
            }).then(()=>{
              this.setState({
                loadingTasks:false
            })
          })
            .catch((e)=>{
                this.setState({
                  loadingTasks:false,
                  err2: e
                })
            })
          }
      

       render(){
        
        return ( 
              <div className="cover"><Topbar/>
            <div className="App">
           
                      

                    <div className="row">
                    
                            <i className="fas fa-clipboard-list">&nbsp;{this.state.list.name}</i>
                            {this.state.list.tasks}
                            
                            
                            
              
            </div>
            </div>
            </div>
         );
}
}
export default ListDetail;