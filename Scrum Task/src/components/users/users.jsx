import React from 'react';
import Topbar from '../../topbar/Topbar';
import './users.css';
import axios from 'axios';
import loh from './ui.png'
import SetRole from '../forms/setRole';
// import lof from './lof.jpg'

  
  export default class Users extends React.Component {
    constructor(props){
      super(props);
      this.state={
        users:[]
      }
    }

    
    getUsers(){
  axios.get('/users')
  .then((r)=> {
      this.setState({
          users: r.data,
          err:''
      })
      console.log(this.state.users);

  })
  .then((r)=>{
    
  })
  .catch((e)=>{
      this.setState({
          err: e
      })
  })
}

componentDidMount(){
  this.getUsers();
  console.log(this.state);
  const profil=localStorage.getItem('profile');
  if(profil===null){
    this.props.history.push('/login')
}
}
    render(){
    return (<div><Topbar/>
      <div className="flex flex-col top">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table className="min-w-full divide-y divide-gray-200 wi">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Name
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Role
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Status
                  </th>
                  
                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {this.state.users.map((person) => (
                  <tr key={person.email}>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="flex items-center">
                        <div className="flex-shrink-0 h-10 w-10">
                          <img className="h-10 w-15 rounded-full" src={loh} alt="pdp" />
                        </div>
                        <div className="ml-4">
                          <div className="text-sm font-medium text-gray-900">{person.name}</div>
                          <div className="text-sm text-gray-500">{person.email}</div>
                        </div>
                      </div>
                    </td>
                    
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="px-6 py-3 whitespace-nowrap text-sm text-gray-500">{person.role}</div>
                      <div className="text-sm text-gray-500">{person.department}</div>
                    </td>
                    
                    <td className="px-6 py-4 whitespace-nowrap">
                      <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Active
                      </span>
                    </td>
                    
                    
                    <td className="px-6 py-4 whitespace-nowrap">
                    <td>
                      <i className="far fa-comment-dots px-2 inline-flex leading-7 font-semibold rounded-full bg-red-200 text-blue-800"></i>
                    </td>
                    <td>
                      <span>
                        <SetRole
                        id={person._id}
                        role={person.role}
                        />
                      </span>
                      </td>
                    </td>
                   
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div></div>
    )
  }
}