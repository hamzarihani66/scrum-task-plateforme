import axios from "axios";
import React from "react";
import loginImg from "./login.svg";
import SimpleReactValidator from 'simple-react-validator';

export class Register extends React.Component {
  constructor(props) {
    super(props.props);
    this.validator = new SimpleReactValidator();
    this.state={
              
      name:'',
      email:'',
      password:'',
      isLogin:false
    }
    this.handleChangeEmail=this.handleChangeEmail.bind(this);
    this.handleChangePassword=this.handleChangePassword.bind(this);

  }
  
  handleChangeName= (e)=>{
    this.setState({
      name:e.target.value

    });
  }
  handleChangeEmail= (e)=>{
    this.setState({
      email:e.target.value

    });
  }
  handleChangePassword= (e)=>{
    this.setState({
      password:e.target.value

    });
  }
  handleRegister=(e)=>{
    if (this.validator.allValid()) {
      axios.post('/users/register',{
        name:this.state.name,
        email:this.state.email,
        password:this.state.password
      }).then(response=>{
        const newUser= {
          name:this.state.name,
          email:this.state.email,
          password:this.state.password }
          if(!response.data.success){
            alert("user already existe pass an other email")
          }else{
            this.setState({...newUser,...response.data})
            alert("your account created successfly")
            this.props.props.history.push('/')
          }
         
        
      }).catch((error) => {
        console.log(error);
      });
    } 

    else {
      this.validator.showMessages();
      this.forceUpdate();
    }

    
  }

  render() {
    return (<from >
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Register</div>
        <div className="content">
          <div className="image">
            <img src={loginImg} alt="loginImg"/>
          </div>
          <div className="form">
            <div className="form-group">
              <label htmlFor="name">Username</label>
              <input type="text" name="name" placeholder="username" value={this.state.name} onChange={this.handleChangeName}/>
              {this.validator.message('name', this.state.name, 'required|alpha|min:4|max:20',{ className: 'text-danger' })}
            </div>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input type="text" name="email" placeholder="email" value={this.state.email} onChange={this.handleChangeEmail}/>
              {this.validator.message('email', this.state.email, 'required|email', { className: 'text-danger' })}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input type="password" name="password" placeholder="password" value={this.state.password} onChange={this.handleChangePassword}/>
              {this.validator.message('password', this.state.password, 'required|password|min:8|max:40', { className: 'text-danger' })}
            </div>
          </div>
        </div>
        <div className="footer">
          <button type="button" className="btnn" onClick={this.handleRegister.bind(this)}>
            Register
          </button>
        </div>
      </div></from>
    );
  }
}
