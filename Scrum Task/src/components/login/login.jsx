import axios from "axios";
import React from "react";
import loginImg from "../login/login.svg";
import SimpleReactValidator from 'simple-react-validator';
import "./style.css";

export class Login extends React.Component {
          constructor(props) {
            super(props.props);
            this.validator = new SimpleReactValidator();
            this.state={
            
              email:'',
              password:'',
              isLoggined:false
            }
            this.handleChangeEmail=this.handleChangeEmail.bind(this);
            this.handleChangePassword=this.handleChangePassword.bind(this);

          }
          
          handleChangeEmail= (e)=>{
            this.setState({
              email:e.target.value

            });
          }
          handleChangePassword= (e)=>{
            this.setState({
              password:e.target.value

            });
          }
          

          handleLogin = (e) => {
            e.preventDefault();
            if (this.validator.allValid()) {
                axios.post('/users/login',{
                  email:this.state.email,
                  password:this.state.password
              }).then(response =>{
                if(response.data.success){
                  localStorage.setItem('profile', JSON.stringify(response.data));
                  this.props.props.history.push('/')
                  
                }else{
                  alert(response.data.message)
                }
                
              }).catch(error=>{
                  console.log(error);
              })
            }else{
              this.validator.showMessages();
              this.forceUpdate();
            }
            
          }

  render() {
    return (<form>
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Login</div>
        <div className="content">
          <div className="image">
            <img src={loginImg} alt="loginImg" />
          </div>
          <div className="form">
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input id="email" type="text" name="email" placeholder="email" value={this.state.email} onChange={this.handleChangeEmail}/>
              {this.validator.message('email', this.state.email, 'required|email', { className: 'text-danger' })}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input id="password" type="password" name="password" placeholder="password" value={this.state.password} onChange={this.handleChangePassword} />
              {this.validator.message('password', this.state.password, 'required|password|min:8|max:40', { className: 'text-danger' })}
            </div>
          </div>
        </div>
        <div className="footer">
          <button type="button" className="btnn" onClick={this.handleLogin.bind(this)}>
            Login
          </button>
        </div>
      </div></form>
    );
  }
}
export default Login;