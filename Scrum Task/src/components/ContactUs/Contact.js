import './Contact.css'
import Footer from '../Footer/Footer';
import Topbar from '../../topbar/Topbar';

function Contact() {
        return ( <div><Topbar/>
        <section className="contact">
            <div className="content">
                <h2>Contact Us</h2><br/>
                <p>For more information or report for bug contact us</p>
            </div>
            <div className="contain">
                <div className="contactInfo">
                    <div className="box">
                        <div className="icon"><i className="fa fa-map-marker"></i></div>
                        <div className="text">
                            <h3>Address</h3><br/>
                            <p>711 Résidence Diar El Médina 2<br/>
                                - Ezzahra 2034 Tunisie</p>
                        </div>
                    </div>
                    <div className="box">
                        <div className="icon"><i className="fa fa-phone" aria-hidden="true"></i></div>
                        <div className="text">
                            <h3>Phone</h3><br/>
                            <p>+216 25 649 523</p>
                        </div>
                    </div>
                    <div className="box">
                        <div className="icon"><i className="fa fa-envelope"></i></div>
                        <div className="text">
                            <h3>Email</h3>
                            <p>task-manager@winshot.net</p>
                        </div>
                    </div>
                </div>
                <div className="contactForm">
                    <form>
                        <h2>Send Message</h2>
                        <div className="inputBox">
                                <input type="text" name="" required="required"/>
                                <span>Full Name</span>
                        </div>
                        <div className="inputBox">
                                <input type="text" name="" required="required"/>
                                <span>Email</span>
                        </div>
                        <div className="inputBox">
                            <textarea required="required"></textarea>   
                             <span>Type your Message...</span>
                        </div>
                        <div >
                            <button type="button" class="btn btn-info">Send</button>
                        </div>
                    </form>
                </div>
            </div>
           
        </section> 
            <Footer/>
        </div>
        );
    }

export default Contact;