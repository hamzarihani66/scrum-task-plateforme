import React, { Component } from 'react';
import Topbar from '../../topbar/Topbar';
import logo from './ui.png';
import axios from 'axios';
import SimpleReactValidator from 'simple-react-validator';
import './profil.css';
class Profil extends Component {
    constructor(props) {
        super(props);
        this.validator=new SimpleReactValidator();
        this.state = { 
            ConnectedUser:JSON.parse(localStorage.getItem('profile')).user,
            user:[]

         }
    }
    handleInput(e) { 
        this.setState({
         [e.target.name]: e.target.value
        })
    }

    componentDidMount(){
        this.getUser(this.state.ConnectedUser._id);
      }
      
    getUser(id){
        axios.get(`/users/${id}`)
        .then((r)=> {
            this.setState({
                name:r.data.name,
                email:r.data.email,
                user: r.data,
                err:''
            })
            
        })
        .then((r)=>{
            
            
        })
        .catch((e)=>{
            this.setState({
                err: e
            })
        })
        }

    handleClick = id => {
        if(this.validator.allValid()){
        axios.put(`/users/${id}`, {
          name:this.state.name,
          email:this.state.email
        })
        .then((response)=> {
            this.getUser(id)
            
        
        })
        .catch((error)=> {
          console.log(error);
        });
      }else{
        this.validator.showMessages();
        this.forceUpdate();
      }}
      deleteUser = (id) => {
        axios.delete('/users/'+id,{headers:{
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
        }})
        .then((response) =>{
            localStorage.clear();
            this.props.history.push('/')
        })
        .then(()=>{
          
        })
        .catch(function (error) {
          console.log(error);
        });
      
      }

    render() { 
        return (
             <div>
            <Topbar/>
            
            
            <div className="wrapper">
                <div className="left">
                    <img src={logo} 
                    alt="user" width="100" className="imgrad"/>
                    <h4>{this.state.user.name}</h4>
                    <p>{this.state.user.role}</p>
                </div>
            <div className="right">
                <div className="info">
                    <h3>Personel Information</h3>
                    <div className="info_data">
                        <form className="form-inline data">
                        
                        
                            <h4>Name</h4>
                            <input type="text" name="name" className="form-control" defaultValue={this.state.name}
                             onChange={this.handleInput.bind(this)}/>
                            {this.validator.message('user name', this.state.name, 'required|min:3|max:20', { className: 'text-danger' })}
                             <br/>
                            <h4>Email</h4>
                            <input type="text" name="email" className="form-control" defaultValue={this.state.email} 
                            onChange={this.handleInput.bind(this)}/>
                        {this.validator.message('email', this.state.email, 'required|email', { className: 'text-danger' })}
                    </form>
                    </div>
                </div>
            
            
            
                <div >
                <button className="btn btn-primary btn-sm actions" onClick={()=>this.handleClick(this.state.user._id)}>Update</button>                
                <button className="btn btn-danger btn-sm actions" onClick={()=>{if(window.confirm('Are you sure to delete your account ?')){this.deleteUser(this.state.user._id)}}}>Delete my account</button>

                </div>
            </div>
        </div>
                    
                
                    </div>
         );
    }
}
 
export default Profil;