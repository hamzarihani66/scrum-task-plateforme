import React, { Component } from 'react';
import Footer from '../Footer/Footer';
import find from './find.png';
import './Home.css';
import Topbar from '../../topbar/Topbar';
class Home extends Component {
    render() { 
        return (<div>
            <Topbar />
             <div className="home">
            <h1>Find out why</h1>
            <h2>We’ll start at the top</h2> 
            <img src={find} alt="img"/>
            
            </div>
            <Footer/>
            </div>
         );
    }
}
 
export default Home;