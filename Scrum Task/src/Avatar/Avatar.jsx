import React from 'react';
import { Link } from 'react-router-dom';
import pdp from './ui.png';
import { useHistory } from 'react-router';


const Avatar =() =>{
      const history=useHistory();
function logout(){
  localStorage.clear();
  history.push('/login')
 
}
  
        return ( 
        
        <main>
            <div className="container">
                <div className="dropdown text-end">
          <Link to="#" className="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <img src={pdp} alt="pdf" width="40" height="40" className="rounded-circle"/>
          </Link>
          <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
            <li><Link to="/myprofil" className="dropdown-item" >Profil</Link></li>
            <li><hr className="dropdown-divider"/></li>
            <li><button  className="dropdown-item" onClick={()=>{logout()}} >Log out</button></li>
          </ul>
        </div>
            </div>
        </main>
         );
    
}
 
export default Avatar;